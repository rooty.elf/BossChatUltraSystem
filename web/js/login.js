$(document).ready(function () {
    var $registerWindow = $("#register-window");
    var $loginWindow = $("#login-window");
    var $bg = $(".background");
    var $icon = $("#login_icon");
    var $windows = $(".window");
    var $window = $(window);

    $loginWindow.submit(function (){
        var formData = $(this).serializeObject();

        $.ajax({
            url: 'login',
            type: "POST",
            data: formData,
            success: function (data) {
                if(data.status === "USER_NOT_FOUND"){
                    $loginWindow.hide();
                    $registerWindow.show();
                    $("#register-username").html(formData.username);
                    $("#register_password").focus();
                }
                else document.location.href = "page.html";
            },
            error: function (data) {
                data = data.responseJSON;
                switch (data.status) {
                    case "WRONG_PASSWORD":
                        showSnackbar("The password is not correct");break;
                    case "BAD_PARAMETERS":
                        showSnackbar("Data could not be validated");break;
                }
                $('input[name=password]').val("");
            }
        });
        return false;
    });

    $registerWindow.submit(function (){
        var loginFormData = $loginWindow.serializeObject();
        var formData = $(this).serializeObject();

        if(loginFormData.password === formData.password) {
            formData.username = loginFormData.username;
            $.ajax({
                url: 'register',
                type: "POST",
                data: formData,
                success: function (data) {
                    switch (data.status) {
                        case "REGISTERED":
                            showSnackbar("The user " + formData.username + " was registered");break;
                    }
                },
                error: function (data) {
                    data = data.responseJSON;
                    switch (data.status) {
                        case "USER_EXISTS":
                            showSnackbar("The username is not available anymore");break;
                        case "BAD_PARAMETERS":
                            showSnackbar("Data could not be validated");break;
                    }
                }
            });
        }
        else {
            showSnackbar("The passwords did not match");
            $('input[name=password]').val("");
        }
        $registerWindow.hide();
        $loginWindow.show();
        $("#login_password").focus();
        return false;
    });

    $window.resize(function () {
        if($window.width() < 640 || $window.height() < 550){
            $windows.css("width","100%");
            $windows.css("height","100%");
            $windows.css("border","none");
            $bg.hide();
            $icon.hide();
        }
        else {
            $windows.css("width","");
            $windows.css("height","");
            $windows.css("border","");
            $bg.show();
            $icon.show();
        }
    });
    $window.resize();
});
$.ajax({
    url: 'sessionInfo',
    type: "GET",
    success: function (data) {
        if(data.auth) document.location.href = "page.html";
    }
});