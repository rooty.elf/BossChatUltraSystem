//Plagiarized from https://www.w3schools.com/howto/howto_js_snackbar.asp
function showSnackbar(text,duration){
    duration = duration | 3000;
    // Get the snackbar DIV
    var snackbar = document.getElementById("snackbar");

    snackbar.innerText = text;

    // Add the "show" class to DIV
    snackbar.className = "show";

    // After duration, remove the show class from DIV
    setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, duration);
}

var entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
};

function truncateString(string,length){
    length = length | 60;
    if(string.length > length){
        return string.substring(0,length) + "...";
    }
    return string;
}

//Plagiarized from https://stackoverflow.com/a/12034334/9377801
function escapeHtml (string) {
    return String(string).replace(/[&<>"'`=\/]/g, function (s) {
        return entityMap[s];
    });
}

function closeModal(element){
    do {
        if(element.classList.contains("w3-modal")){
            element.style.display='none';
            break;
        }
    }
    while (element = element.parentNode);
}


