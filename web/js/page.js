var username;
var messages;

$(document).ready(function () {
    $.ajax({
        url: 'sessionInfo',
        type: "GET",
        success: function (data) {
            if(data.auth){
                username = data.username;
                document.getElementById("user-text").innerText = username;
                fetchMessages();
                initFormActions();
            }
            else {
                document.location.href = "index.html";
            }
        },
        error: function (data) {
            document.location.href = "index.html";
        }
    });

    initButtonActions();
    initSearchBar();

    var $window = $(window);
    $window.resize(function () {
        var mainContent = document.getElementById("main-content");
        if(window.innerWidth < 993){
            mainContent.style.height = window.innerHeight - 56 + "px";
        }
        else {
            mainContent.style.height = "";
        }
    });
    $window.resize();
});

function showMessages(messages,container){
    container.innerHTML = "";

    for(var m in messages){
        var message = messages[m];
        if(message) $(container).append(buildMessage(message))
    }
}


function buildMessage(message) {
    var $message = $(
        "<a class=\"w3-bar-item w3-button w3-border-bottom test w3-hover-light-grey\">" +
        "    <div class=\"w3-container\">" +
        "        <img class=\"w3-round w3-margin-right\" src=\"assets/avatar.png\"" +
        "             style=\"width:15%;\"><span class=\"w3-opacity w3-large\">" + escapeHtml(truncateString(message.author)) + "</span>" +
        "        <h6>" + escapeHtml(truncateString(message.subject,20)) + "</h6>" +
        "        <p>" + escapeHtml(truncateString(message.text)) + "</p>" +
        "    </div>" +
        "</a>"
    );

    $message.click(function () {
        openMessage(message);
        w3_close();
    });

    return $message;
}

function openMessage(message) {
    var $mainContent = $("#main-content");

    var $message = $("<div class='w3-container person'>");
    $message.append("<br>");
    $message.append("<img class='w3-round  w3-animate-top' src='assets/avatar.png' style='width:20%;'>");
    $message.append("<h5 class='w3-opacity'>Subject: " + escapeHtml(message.subject) + "</h5>");
    $message.append("<h4><i class='fas fa-clock-o'></i> From " + escapeHtml(message.author) + ", " + escapeHtml(message.time) + ".</h4>");
    if(message.author === username){
        $message.append("<div class='message-button-container'><a class='w3-button w3-light-grey message-button' onclick='editMessage(" + escapeHtml(JSON.stringify(message)) + ")'>Edit<i class='w3-margin-left fas fa-pencil-alt'></i></a><div><a class='w3-button w3-light-grey message-button' onclick='deleteMessage(" + message.id + ")'>Delete<i class='w3-margin-left fas fa-trash'></i></a></div>");
    }
    $message.append("<hr>");
    $message.append("<p class='message-text' style='white-space: pre-wrap'>" + escapeHtml(message.text).replace(/(^|\s)(#[^\\,^\\.^\s-]+)/ig, "$1<a href='javascript:doSearch(\"$2\")'>$2</a>") + "</p>");

    $mainContent.empty();
    $mainContent.append($message);
}

function editMessage(message){
    var $mainContent = $("#main-content");

    var $message = $("<form class='w3-container person'>");
    $message.append("<br>");
    $message.append("<img class='w3-round  w3-animate-top' src='assets/avatar.png' style='width:20%;'>");
    $message.append("<h5 class='w3-opacity'>Subject: " + escapeHtml(message.subject) + "</h5>");
    $message.append("<h4><i class='fas fa-clock-o'></i> From " + escapeHtml(message.author) + ", " + escapeHtml(message.time) + ".</h4>");
    $message.append("<div class='message-button-container'><button class='w3-button w3-light-grey message-button' type='submit'>Submit<i class='w3-margin-left fas fa-paper-plane'></i></button></div>");
    $message.append("<hr>");
    $message.append("<textarea class='message-text' name='text'style='white-space: pre-wrap'>" + escapeHtml(message.text) + "</textarea>");


    $mainContent.empty();
    $message.submit(function () {
        updateMessage($(this).serializeObject().text,message);
        return false;
    });
    $mainContent.append($message);
}

function updateMessage(text, message) {
    message.text = text;
    $.ajax({
        url: 'messageupdate',
        type: "POST",
        data: message,
        success: function (data) {
            openMessage(message);
            showSnackbar("Message updated");
            fetchMessages();
        },
        error: function (data) {
            data = data.responseJSON;
            switch (data.status) {
                case "BAD_PARAMETERS":
                    showSnackbar("There was an error while deleting the message");break;
                case "SESSION_INVALID":
                    showSnackbar("There is no valid session for this request");break;
                case "MESSAGE_NOT_FOUND":
                    showSnackbar("The message was not found");break;
                case "USER_NOT_AUTHOR_OF_MESSAGE":
                    showSnackbar("You are not the owner of this message");break;
                default:
                    showSnackbar("There was an error");break;
            }
        }
    });
}

function deleteMessage(id){
    $.ajax({
        url: 'messagedelete',
        type: "POST",
        data: {id:id},
        success: function (data) {
            $("#main-content").empty();
            showSnackbar("Message deleted");
            fetchMessages();
        },
        error: function (data) {
            data = data.responseJSON;
            switch (data.status) {
                case "BAD_PARAMETERS":
                    showSnackbar("There was an error while deleting the message");break;
                case "SESSION_INVALID":
                    showSnackbar("There is no valid session for this request");break;
                case "MESSAGE_NOT_FOUND":
                    showSnackbar("The message was not found");break;
                case "USER_NOT_AUTHOR_OF_MESSAGE":
                    showSnackbar("You are not the owner of this message");break;
            }
        }
    });
}

function initButtonActions(){
    document.getElementById("btn-logout").onclick = logout;
    $("#btn-public").click(function (e) {
        handleSideElement('cont-public');
    });

    $("#btn-inbox").click(function (e) {
        handleSideElement('cont-inbox');
    });

    $("#btn-sent").click(function (e) {
        handleSideElement('cont-sent');
    });

    $("#btn-notes").click(function (e) {
        handleSideElement('cont-notes');
    });

    $("#nav-search").click(function (e) {
        hideSideElement(document.getElementById('cont-search'));
    });
}


function search(term) {
    var results = [];
    var messageCollections = [messages.public,messages.private,messages.sent,messages.notes];

    var tag = term.startsWith("#");
    if(tag) term = term.substr(1);

    for(var i in messageCollections){
        for(var j in messageCollections[i]){
            var message = messageCollections[i][j];
            var match = false;
            if(tag){
                for(var k in message.tags){
                    if(message.tags[k] === term){
                        match = true;
                        break;
                    }
                }
            }
            else{
                match = message.subject.toLowerCase().replace(" ","").includes(term.toLowerCase());
            }
            if(match) results.push(message);
        }
    }
    return results;
}

function handleSideElement(id) {
    var element = document.getElementById(id);
    if (element.className.indexOf("w3-show") == -1) {
        showSideElement(element);
    } else {
        hideSideElement(element);
    }
}

function showSideElement(element){
    element.className += " w3-show";
    element.previousElementSibling.className += " w3-red";
}

function hideSideElement(element){
    element.className = element.className.replace(" w3-show", "");
    element.previousElementSibling.className = element.previousElementSibling.className.replace(" w3-red", "");
}

function initSearchBar() {
    var input = document.getElementById("searchField");

    input.addEventListener("keyup", function(event) {
        if (event.key === "Enter") {
            var term = input.value;
            if(term === ""){
                hideSideElement(document.getElementById("cont-search"));
            }
            else{
                doSearch(term);
            }
        }
    });
}

function doSearch(term){
    var contSearch = document.getElementById("cont-search");
    showMessages(search(term),contSearch);
    showSideElement(contSearch);
}

function initFormActions() {
    $("#form-post").submit(function () {
        var form = this;
        $.ajax({
            url: 'message',
            type: "POST",
            data: $(form).serialize(),
            success: function (data) {
                console.log(data);
                closeModal(form);
                showSnackbar("Message sent");
                fetchMessages();
                form.reset();
            },
            error: function (data) {
                data = data.responseJSON;
                if(data.status === "RECIPIENT_NOT_FOUND")
                    showSnackbar("The recipient was not found");
                else showSnackbar("There was an error while processing this message");
            }
        });
        return false;
    });

    $("#form-settings").submit(function () {
        var form = this;
        var formData = $(form).serializeObject();
        if(formData.new !== formData.repeat){
            showSnackbar("the passwords do not match");
            return false;
        }

        $.ajax({
            url: 'changepassword',
            type: "POST",
            data: {password: formData.new,current: formData.current},
            success: function (data) {
                closeModal(form);
                showSnackbar("Password changed");
                form.reset();
            },
            error: function (data) {
                data = data.responseJSON;
                if(data.status === "WRONG_PASSWORD") showSnackbar("wrong password");
                else showSnackbar("Error while changing password");
            }
        });
        return false;
    });
}

function fetchMessages(){
    $.ajax({
        url: 'message',
        type: "GET",
        success: function (data) {
            messages = data;
            showMessages(messages.public,document.getElementById("cont-public"));
            showMessages(messages.private,document.getElementById("cont-inbox"));
            showMessages(messages.sent,document.getElementById("cont-sent"));
            showMessages(messages.notes,document.getElementById("cont-notes"));
        },
        error: function (data) {
            showSnackbar("Messages could not be refreshed")
        }
    });
}

function logout(){
    $.ajax({
        url: 'logout',
        type: "POST",
        success: function (data) {
            document.location.href = "index.html";
        },
        error: function (data) {
        }
    });
}

//W3 template stuff
function w3_open() {
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}