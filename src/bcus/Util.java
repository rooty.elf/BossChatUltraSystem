package bcus;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Function;

/**
 * A collection of helper functions.
 */
public class Util {
    /**
     * Gets a certain cookie value ot of a HttpServletRequest.
     *
     * @param req The http request
     * @param key The key of the desired cookie value
     * @return the cookie value
     */
    public static String getCookieValue(HttpServletRequest req, String key){
        if(req.getCookies() == null) return null;
        for (Cookie cookie : req.getCookies()) {
            if(cookie.getName().equals(key)) return cookie.getValue();
        }
        return null;
    }

    /**
     * This function resceives a Collection of a certain type T and
     * a Function which transforms an instance of T to an instance of
     * a certain type U. The function returns a Collection of U which was
     * created based on the input collection and the transformation function.
     *
     * @param input The input Collection
     * @param transform The transformation Function
     * @param <T> The type of the input Collection
     * @param <U> The type of the output Collection
     * @return The output Collection of type U
     */
    public static <T,U> Collection<U> transform(Collection<T> input, Function<T,U> transform){
        Collection<U> output = new ArrayList<>();
        for (T t : input) {
            output.add(transform.apply(t));
        }
        return output;
    }
}