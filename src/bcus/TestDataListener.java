package bcus;

import bcus.model.Message;
import bcus.model.User;

import javax.persistence.EntityManager;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class TestDataListener  implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        EntityManager em = EntityManagerFactory.getInstance();
        if (em.createQuery("select count(u) from User u").getSingleResult().equals(0L)) {
            System.out.println("Insert test data");

            User mario = new User("john", "john");
            User li = new User("li", "li");

            em.getTransaction().begin();
            em.persist(mario);
            em.persist(li);

            em.persist(new Message(mario, li, "Hello", "Hi"));
            em.persist(new Message(li, mario, "Hi, how's it going?", "Hi"));
            em.persist(new Message(mario, li, "Good", "Hi"));
            em.persist(new Message(li, mario, "And how is your assignment doing? What was it called? Boss Chat Ultra System? That's quite a good name. Very professional.", "Hi"));
            em.persist(new Message(mario, li, "Oh, it's doing good.", "Boss Chat Ultra System"));
            em.persist(new Message(li, mario, "Ah, I see. This actually looks like a very high quality piece of software", "Boss Chat Ultra System"));
            em.persist(new Message(mario, li, "Yes, I have only used best practises and the latest technologies.", "Boss Chat Ultra System"));
            em.persist(new Message(li, mario, "I know that I have said before, that I wouldn't give a #100%, but maybe I need to think about this again.", "Boss Chat Ultra System"));
            em.persist(new Message(mario, null, "PSA: The Ultra System is now live. Come and join the revolution of communication!", "Boss Chat Ultra System"));
            em.persist(new Message(li, li, "Mark for Boss Chat Ultra System: #100%.", "Boss Chat Ultra System"));
            em.persist(new Message(mario, null, "Did you know, that BCUS features responsive webdesign? Now you can go all #mobile.", "Responsive"));
            em.persist(new Message(li, null, "This is #100% great!.", "Responsive"));
            em.getTransaction().commit();
        }
    }
}
