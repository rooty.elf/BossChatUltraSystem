package bcus.model;

import org.json.JSONObject;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@NamedQueries({
        @NamedQuery(
                name = Message.getPublicMessages,
                query = "SELECT m FROM Message m WHERE m.recipient = NULL order by m.time desc"
        ),
        @NamedQuery(
                name = Message.getPrivateForUser,
                query = "SELECT m FROM Message m WHERE m.author != :user and m.recipient = :user order by m.time desc"
        ),
        @NamedQuery(
                name = Message.getSentForUser,
                query = "SELECT m FROM Message m WHERE m.author = :user and m.recipient != :user order by m.time desc"
        ),
        @NamedQuery(
                name = Message.getNotesForUser,
                query = "SELECT m FROM Message m WHERE m.author = :user and m.recipient = :user order by m.time desc"
        )
})

@Entity
public class Message {
    public static final String getPublicMessages = "Message.getPublicMessages";
    public static final String getPrivateForUser = "Message.getPrivateForUser";
    public static final String getSentForUser = "Message.getSentForUser";
    public static final String getNotesForUser = "Message.getNotesForUser";


    @Id
    @GeneratedValue
    private long id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn
    private User author;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn
    private User recipient;

    @ElementCollection
    private Set<String> tags;

    private Date time;

    @Column(columnDefinition = "TEXT")
    private String text;

    private String subject;

    public Message() {
        tags = new HashSet<>();
        time = new Date();
    }


    public Message(User author, User recipient, String text, String subject) {
        this();
        this.author = author;
        this.recipient = recipient;
        this.subject = subject;
        setText(text);
    }

    public void setText(String text){
        tags.clear();

        for(String word : text.split("[\\s.,]")){
            if(word.startsWith("#")){
                tags.add(word.substring(1));
            }
        }

        this.text = text;
    }

    public String getText() {
        return text;
    }

    public JSONObject toJSON(){
        JSONObject jo = new JSONObject();
        jo.put("id", id);
        jo.put("author", author.getUsername());
        jo.put("subject", subject);
        jo.put("text",text);
        jo.put("time",time);
        jo.put("tags",tags);
        return jo;
    }

    public User getAuthor() {
        return author;
    }
}
