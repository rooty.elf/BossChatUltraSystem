package bcus.model;

import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "ACCOUNT")
public class User {
    @Id
    private String username;

    @Column(nullable = false)
    private String password;

    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "recipient")
    private Collection<Message> messages;

    public User() {
    }

    public User(String username, String password) {
        this.username = username.toLowerCase();
        setPassword(password);
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public boolean checkPassword(String password) {
        return BCrypt.checkpw(password, this.password);
    }
}
