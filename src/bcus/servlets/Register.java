package bcus.servlets;

import bcus.EntityManagerFactory;
import bcus.model.User;
import org.json.JSONObject;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Register", urlPatterns = {"/register"})
public class Register extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=utf-8");
        int status = HttpServletResponse.SC_BAD_REQUEST;
        JSONObject rspData = new JSONObject();
        EntityManager em = null;
        try {
            em = EntityManagerFactory.getInstance();
            String username = req.getParameter("username");
            String password = req.getParameter("password");

            if (username != null && !username.isEmpty() && password != null && !password.isEmpty()) {
                username = username.toLowerCase();
                User user = new User(username, password);
                if (em.find(User.class, username) == null) {
                    em.getTransaction().begin();
                    em.persist(user);
                    em.getTransaction().commit();
                    status = HttpServletResponse.SC_CREATED;

                    rspData.put("status",Status.REGISTERED);
                }
                else rspData.put("status", Status.USER_EXISTS);
            }
            else rspData.put("status",Status.BAD_PARAMETERS);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        resp.setStatus(status);
        resp.getWriter().write(rspData.toString());
    }

    private enum Status{
        BAD_PARAMETERS, USER_EXISTS, REGISTERED
    }
}
