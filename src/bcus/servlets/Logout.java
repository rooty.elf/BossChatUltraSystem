package bcus.servlets;

import bcus.session.SessionManager;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

import static bcus.Util.getCookieValue;

@WebServlet(name = "Logout", urlPatterns = {"/logout"})
public class Logout extends HttpServlet {
    private SessionManager sessionManager;

    @Override
    public void init() throws ServletException {
        sessionManager = SessionManager.getInstance();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JSONObject rspData = new JSONObject();
        resp.setContentType("application/json;charset=utf-8");
        int status = HttpServletResponse.SC_BAD_REQUEST;

        String sessionString = getCookieValue(req, "sessionId");

        if (sessionString != null) {
            if(sessionManager.terminateSession(UUID.fromString(sessionString)) != null)
                 rspData.put("status", Status.LOGGED_OUT);
            else rspData.put("status", Status.SESSION_NOT_FOUND);

            status = HttpServletResponse.SC_OK;
        }
        else rspData.put("status", Status.BAD_SESSION_DATA);

        resp.setStatus(status);
        resp.getWriter().write(rspData.toString());
    }

    private enum Status {
        BAD_SESSION_DATA, SESSION_NOT_FOUND, LOGGED_OUT
    }
}