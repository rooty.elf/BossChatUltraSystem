package bcus.servlets;

import bcus.EntityManagerFactory;
import bcus.model.User;
import bcus.session.SessionManager;
import org.json.JSONObject;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet(name = "Login", urlPatterns = {"/login"})
public class Login extends HttpServlet {
    private SessionManager sessionManager;

    @Override
    public void init() throws ServletException {
        sessionManager = SessionManager.getInstance();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JSONObject rspData = new JSONObject();
        resp.setContentType("application/json;charset=utf-8");
        int status = HttpServletResponse.SC_BAD_REQUEST;
        EntityManager em = null;
        try {
            em = EntityManagerFactory.getInstance();
            String username = req.getParameter("username");
            String password = req.getParameter("password");

            if (username != null && password != null) {
                User user = em.find(User.class, username.toLowerCase());
                if (user != null) {
                    if (user.checkPassword(password)) {
                        UUID sessionId = sessionManager.createSession(user.getUsername());
                        resp.addCookie(new Cookie("sessionId", sessionId.toString()));
                        status = HttpServletResponse.SC_OK;
                        rspData.put("status",Status.LOGGED_IN);
                        rspData.put("auth",true);
                        rspData.put("username",sessionManager.lookupUser(sessionId));
                    }
                    else rspData.put("status",Status.WRONG_PASSWORD);
                }
                else {
                    rspData.put("status", Status.USER_NOT_FOUND);
                    status = HttpServletResponse.SC_OK;
                }
            }
            else rspData.put("status",Status.BAD_PARAMETERS);
        } finally {
            if (em != null) {
                em.close();
            }
        }
        resp.setStatus(status);
        resp.getWriter().write(rspData.toString());
    }

    private enum Status {
        LOGGED_IN, USER_NOT_FOUND, WRONG_PASSWORD, BAD_PARAMETERS
    }
}