package bcus.servlets;

import bcus.EntityManagerFactory;
import bcus.model.Message;
import bcus.model.User;
import bcus.session.SessionManager;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static bcus.Util.transform;
import static bcus.Util.getCookieValue;

@WebServlet(name = "MessageDispatch", urlPatterns = {"/message"})
public class MessageDispatch extends HttpServlet {
    private SessionManager sessionManager;

    @Override
    public void init() throws ServletException {
        sessionManager = SessionManager.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JSONObject rspData = new JSONObject();
        resp.setContentType("application/json;charset=utf-8");
        int status = HttpServletResponse.SC_BAD_REQUEST;

        EntityManager em = null;
        try {
            String sessionString = getCookieValue(req, "sessionId");
            if (sessionString != null) {
                String username = sessionManager.lookupUser(UUID.fromString(sessionString));
                if (username != null) {
                    em = EntityManagerFactory.getInstance();

                    List<Message> publicMessages = em.createNamedQuery(Message.getPublicMessages, Message.class)
                            .getResultList();
                    JSONArray pub = new JSONArray(transform(publicMessages, Message::toJSON));

                    User user = em.find(User.class,username);

                    List<Message> privateMessages = em.createNamedQuery(Message.getPrivateForUser, Message.class)
                            .setParameter("user",user).getResultList();
                    JSONArray pri = new JSONArray(transform(privateMessages, Message::toJSON));

                    List<Message> sentMessages = em.createNamedQuery(Message.getSentForUser, Message.class)
                            .setParameter("user",user).getResultList();
                    JSONArray sent = new JSONArray(transform(sentMessages, Message::toJSON));

                    List<Message> noteMessages = em.createNamedQuery(Message.getNotesForUser, Message.class)
                            .setParameter("user",user).getResultList();
                    JSONArray notes = new JSONArray(transform(noteMessages, Message::toJSON));

                    rspData.put("public",pub);
                    rspData.put("private",pri);
                    rspData.put("sent",sent);
                    rspData.put("notes",notes);

                    status = HttpServletResponse.SC_OK;
                    rspData.put("status",Status.MESSAGES_LOADED);
                }
                else rspData.put("status",Status.SESSION_INVALID);
            }
            else rspData.put("status",Status.NOT_AUTHENTICATED);
        } finally {
            if (em != null) {
                em.close();
            }
        }

        resp.setStatus(status);
        resp.getWriter().print(rspData);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JSONObject rspData = new JSONObject();
        resp.setContentType("application/json;charset=utf-8");
        int status = HttpServletResponse.SC_BAD_REQUEST;

        EntityManager em = null;
        try {
            em = EntityManagerFactory.getInstance();

            String messageText = req.getParameter("text");
            String subject = req.getParameter("subject");
            String sessionString = getCookieValue(req, "sessionId");
            String recipientUserName = req.getParameter("recipient");
            recipientUserName = "".equals(recipientUserName) ? null : recipientUserName;

            if (messageText != null && !messageText.isEmpty() && sessionString != null) {
                User user = em.find(User.class,sessionManager.lookupUser(UUID.fromString(sessionString)));
                if (user != null) {
                    User recipient = null;
                    if (recipientUserName != null) {
                        recipientUserName = recipientUserName.toLowerCase();
                        recipient = em.find(User.class, recipientUserName);
                        if (recipient != null){
                            em.refresh(user);
                        }
                        else {
                            status = HttpServletResponse.SC_NOT_FOUND;
                            rspData.put("status",Status.RECIPIENT_NOT_FOUND);
                        }
                    }

                    if (!(recipient == null && recipientUserName != null)){
                        em.getTransaction().begin();
                        em.persist(new Message(user, recipient, messageText, subject));
                        em.getTransaction().commit();
                        status = HttpServletResponse.SC_CREATED;
                        rspData.put("status",Status.MESSAGE_SENT);
                    }
                }
                else rspData.put("status",Status.SESSION_INVALID);
            }
            else rspData.put("status",Status.NOT_AUTHENTICATED);
        } finally {
            if (em != null) {
                em.close();
            }
        }

        resp.setStatus(status);
        resp.getWriter().print(rspData);
    }

    private enum Status{
        NOT_AUTHENTICATED, SESSION_INVALID, RECIPIENT_NOT_FOUND, MESSAGES_LOADED, MESSAGE_SENT
    }
}