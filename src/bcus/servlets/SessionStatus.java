package bcus.servlets;

import bcus.session.SessionManager;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

import static bcus.Util.getCookieValue;

@WebServlet(name = "SessionStatus", urlPatterns = {"/sessionInfo"})
public class SessionStatus extends HttpServlet {
    private SessionManager sessionManager;

    @Override
    public void init() throws ServletException {
        sessionManager = SessionManager.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=utf-8");
        JSONObject rspData = new JSONObject();
        rspData.put("auth",false);
        String sessionString = getCookieValue(req, "sessionId");
        if(sessionString == null) {
            resp.getWriter().write(rspData.toString());
            return;
        }

        String username = sessionManager.lookupUser(UUID.fromString(sessionString));
        if(username != null){
            rspData.put("auth",true);
            rspData.put("username",username);
        }
        resp.getWriter().write(rspData.toString());
    }
}