package bcus.servlets;

import bcus.EntityManagerFactory;
import bcus.model.Message;
import bcus.model.User;
import bcus.session.SessionManager;
import org.json.JSONObject;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

import static bcus.Util.getCookieValue;

@WebServlet(name = "MessageDelete", urlPatterns = {"/messagedelete"})
public class MessageDelete extends HttpServlet {
    private SessionManager sessionManager;

    @Override
    public void init() throws ServletException {
        sessionManager = SessionManager.getInstance();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JSONObject rspData = new JSONObject();
        resp.setContentType("application/json;charset=utf-8");
        int status = HttpServletResponse.SC_BAD_REQUEST;

        EntityManager em = null;
        try {
            em = EntityManagerFactory.getInstance();
            String messageId = req.getParameter("id");
            String sessionString = getCookieValue(req, "sessionId");
            if(messageId != null && sessionString != null){
                long id = Long.parseLong(messageId);
                String username = sessionManager.lookupUser(UUID.fromString(sessionString));
                if(username != null){
                    User caller = em.find(User.class,username);
                    if(caller != null){
                        Message message = em.find(Message.class,id);
                        if(message != null){
                            if(message.getAuthor().getUsername().equals(caller.getUsername())){
                                em.getTransaction().begin();
                                em.remove(message);
                                em.getTransaction().commit();
                                rspData.put("status", Status.MESSAGE_DELETED);
                                status = HttpServletResponse.SC_OK;
                            }
                            else rspData.put("status", Status.USER_NOT_AUTHOR_OF_MESSAGE);
                        }
                        else rspData.put("status", Status.MESSAGE_NOT_FOUND);
                    }
                    else rspData.put("status", Status.SESSION_INVALID);
                }
                else rspData.put("status", Status.SESSION_INVALID);
            }
            else rspData.put("status", Status.BAD_PARAMETERS);
        }
        catch (NumberFormatException e){
            rspData.put("status", Status.BAD_PARAMETERS);
        }
        finally {
            if (em != null) {
                em.close();
            }
        }

        resp.setStatus(status);
        resp.getWriter().print(rspData);
    }

    private enum Status{
        BAD_PARAMETERS, SESSION_INVALID, MESSAGE_NOT_FOUND, USER_NOT_AUTHOR_OF_MESSAGE, MESSAGE_DELETED
    }
}