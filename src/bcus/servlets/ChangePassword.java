package bcus.servlets;

import bcus.EntityManagerFactory;
import bcus.model.User;
import bcus.session.SessionManager;
import org.json.JSONObject;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

import static bcus.Util.getCookieValue;

@WebServlet(name = "ChangePassword", urlPatterns = {"/changepassword"})
public class ChangePassword extends HttpServlet {
    private SessionManager sessionManager;

    @Override
    public void init() throws ServletException {
        sessionManager = SessionManager.getInstance();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json;charset=utf-8");
        int status = HttpServletResponse.SC_BAD_REQUEST;
        JSONObject rspData = new JSONObject();
        EntityManager em = null;

        String sessionString = getCookieValue(req, "sessionId");
        if (sessionString != null) {
            String password = req.getParameter("password");
            String current = req.getParameter("current");
            if (password != null && !password.isEmpty() && current != null) {
                String username = sessionManager.lookupUser(UUID.fromString(sessionString));
                if (username != null) {
                    try {
                        em = EntityManagerFactory.getInstance();
                        User user = em.find(User.class, username);

                        if (user != null) {
                            if (user.checkPassword(current)) {
                                em.getTransaction().begin();
                                user.setPassword(password);
                                em.getTransaction().commit();
                                status = HttpServletResponse.SC_OK;

                                rspData.put("status", Status.PASSWORD_CHANGED);
                            } else rspData.put("status", Status.WRONG_PASSWORD);
                        } else rspData.put("status", Status.INVALID_SESSION);
                    } finally {
                        if (em != null) {
                            em.close();
                        }
                    }
                } else rspData.put("status", Status.INVALID_SESSION);
            } else rspData.put("status", Status.BAD_PARAMETERS);
        } else rspData.put("status", Status.INVALID_SESSION);


        resp.setStatus(status);
        resp.getWriter().write(rspData.toString());
    }

    private enum Status {
        INVALID_SESSION, BAD_PARAMETERS, WRONG_PASSWORD, PASSWORD_CHANGED
    }
}
