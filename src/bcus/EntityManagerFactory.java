package bcus;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class EntityManagerFactory {

    public static EntityManager getInstance() {
        return Persistence.createEntityManagerFactory("boss").createEntityManager();
    }

}
