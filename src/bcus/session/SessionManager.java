package bcus.session;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SessionManager {

    private static SessionManager instance;

    public synchronized static SessionManager getInstance(){
        if(instance == null) instance = new SessionManager();
        return instance;
    }

    private Map<UUID,String> sessions;

    private SessionManager() {
        sessions = new HashMap<>();
    }

    public UUID createSession(String username){
        UUID sessionId = UUID.randomUUID();
        sessions.put(sessionId,username);
        return sessionId;
    }

    public String lookupUser(UUID sessionId){
        return sessions.get(sessionId);
    }

    public String terminateSession(UUID sessionId){
        String username = sessions.get(sessionId);
        sessions.remove(sessionId);
        return username;
    }
}