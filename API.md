# API Doc

## /register
### POST
Registers a new user.
#### Parameters
Field | Type | Description
---|---|---
username|String|The username
password|String|The password
#### Response
##### Status codes
* 201: success - the user was registered

Field | Type | Description
---|---|---
status|String|The status of the request.

Possible status values:  
__REGISTERED__: The user was registered  

* 400: error

Field | Type | Description
---|---|---
status|String|The status of the request.

Possible status values:  
__USER_EXISTS__: The username is already taken  
__BAD_PARAMETERS__: Insufficient parameters were provided  

## /login
### POST
Perform a login and store the session key locally.
#### Parameters

Field | Type | Description
---|---|---
username|String| The username
password|String| The password
#### Response
##### Status codes
* 200: success - the user was logged in

Field | Type | Description
---|---|---
auth|Boolean|Whether the user was authenticated
username|String|The username of the authenticated user
status|String|The status of the request.

Possible status values:  
__LOGGED_IN__: The user was logged in  

* 400: error

Field | Type | Description
---|---|---
status|String|The status of the request.

Possible status values:  
__USER_NOT_FOUND__: The client does not have a valid session  
__BAD_PARAMETERS__: Insufficient parameters were provided  
__WRONG_PASSWORD__: The current password was not correct  

## /logout
### POST
Perform a logout and remove the session key.
#### Response
##### Status codes
* 200: success - the user was logged out

Field | Type | Description
---|---|---
status|String|The status of the request

Possible status values:  
__LOGGED_OUT__: The user was logged out  

* 400: error

Field | Type | Description
---|---|---
status|String|The status of the request

Possible status values:  
__SESSION_NOT_FOUND__: There was no session found  
__BAD_SESSION_DATA__: The provided session information is not valid  

## /changepassword
### POST
Changes the password of the currently logged in user.
#### Parameters
Field | Type | Description
---|---|---
password|String|The new password of the user
current|String|The current password of the user
#### Response
##### Status codes
* 200: success - the password was changed


Field | Type | Description
---|---|---
status|String|The status of the request.

Possible status values:  
__PASSWORD_CHANGED__: The password was changed  

* 400: error

Field | Type | Description
---|---|---
status|String|The status of the request

Possible status values:  
__INVALID_SESSION__: The client does not have a valid session  
__BAD_PARAMETERS__: Insufficient parameters were provided  
__WRONG_PASSWORD__: The current password was not correct  

## /message
### GET
Loads the messages for the currently logged in user.
#### Response
##### Status codes
* 200: success - The messages were loaded

Field | Type | Description
---|---|---
public|Message[]|Public messages
private|Message[]|Messages which were sent to the user
sent|Message[]|Messages which were sent by the user
notes|Message[]|Notes for the user (author and recipient both equal the caller)
status|String|The status of the request

Possible status values:  
__MESSAGES_LOADED__: The messages were loaded  

* 400: error

Field | Type | Description
---|---|---
status|String|The status of the request

Possible status values:  
__SESSION_INVALID__: The client does not have a valid session  
__NOT_AUTHENTICATED__: The caller is not authenticated  

### POST
Sends a new message.
#### Parameters
Field | Type | Description
---|---|---
text|String| The text content of the message
subject|String| The subject of the message
recipient|String| The recipient of the message - empty means public

#### Response
##### Status codes
* 201: success - the message was sent

Field | Type | Description
---|---|---
status|String|The status of the request.

Possible status values:  
__MESSAGE_SENT__: The message was sent

* 400: error

Field | Type | Description
---|---|---
status|String|The status of the request.

Possible status values:  
__SESSION_INVALID__: The client does not have a valid session  
__NOT_AUTHENTICATED__: The caller is not authenticated  
__RECIPIENT_NOT_FOUND__: There was a recipient provided who was not found  
#### Structure of a message
Field | Type | Description
---|---|---
id|Number|The id of the message
author|String|The author of the message
subject|String|The subject of the message
text|String|The text body of the message
time|String|The time the message was sent
tags|String[]|Hashtags within the message

## /messagedelete
### POST
Removes a message.

#### Parameters

Field | Type | Description
---|---|---
id|Number| The ID of the message which should be removed.
#### Response
##### Status codes
* 200: success - the message was deleted

Field | Type | Description
---|---|---
status|String|The status of the request

Possible status values:  
__MESSAGE_DELETED__: The message was deleted  

* 400: error

Field | Type | Description
---|---|---
status|String|The status of the request

Possible status values:  
__SESSION_INVALID__: The client does not have a valid session  
__BAD_PARAMETERS__: Insufficient parameters were provided  
__MESSAGE_NOT_FOUND__: The provided message id does not correspond to any message on the system  
__USER_NOT_AUTHOR_OF_MESSAGE__: The message corresponding to the provided id does not belong to the caller  

## /messageupdate
### POST
Changes the content of a message.
#### Parameters
Field | Type | Description
---|---|---
id|Number|The ID of the message which should be changed
text|String|The new text for the message
#### Response
##### Status codes
* 200: success - The message was updated

Field | Type | Description
---|---|---
status|String|The status of the request

Possible status values:  
__MESSAGE_UPDATED__: The message was updated  

* 400: error

Field | Type | Description
---|---|---
status|String|The status of the request

Possible status values:  
__SESSION_INVALID__: The client does not have a valid session  
__BAD_PARAMETERS__: Insufficient parameters were provided  
__MESSAGE_NOT_FOUND__: The provided message id does not correspond to any message on the system  
__USER_NOT_AUTHOR_OF_MESSAGE__: The message corresponding to the provided id does not belong to the caller  

## /sessionInfo
### GET
Returns the status uf the current session.
#### Response
##### Status codes
* 200: success

Field | Type | Description
---|---|---
auth|Boolean|Whether the user is authenticated
username|String|The username of the logged in user, if applicable


